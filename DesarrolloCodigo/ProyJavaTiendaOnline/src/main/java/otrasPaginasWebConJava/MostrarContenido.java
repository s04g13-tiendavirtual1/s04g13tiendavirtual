package otrasPaginasWebConJava;
 //ConexionDB;

import conexion_db.ConexionDB;
import javax.imageio.IIOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

@WebServlet(name = "mostrarContenido", value = "/mostrar-contenido")

public class MostrarContenido extends HttpServlet {
    public void init(){

    }

    public void doGot(HttpServletRequest req, HttpServletResponse resp) throws IIOException {
        resp.setContentType("text/html");

        PrintWriter imprimir = resp.getWriter();
        imprimir.println("<html><body>");
        imprimir.println("<h1> Hola Tripulantes </h1>");
        imprimir.println("p> Contenido en otra página con un párafo...");
        double suma = ConexionDB.sumaAB(10,6);
        imprimir.println("<h2>" + "El resultado de a + b es = " + suma + "</h2>");
        imprimir.println("</body></html>");

    }

    public void destroy(){

    }
}

# Importar el módulo de flask
from flask import Flask

# utilizar variable para crear la aplicación
app = Flask(__name__)

# ruta inicial cuando abro el proyecto
@app.route('/')
# la ruta está asociada a un método, en este caso el index
def index():
    return "<h1> Hola Tripulantes... </h>"

@app.route('/saludo')
def saludo():
    return "<h1> Programando ando... </h>"

@app.route('/contacto')
@app.route('/contacto/<email>')
def contacto(email = None):
    
    if email != None:
        email = "misiontic.formador17@uis.edu.co"
    else:
        email = ""
    return f"""
    <h1> Correo Electrónico de Contacto: </h1>
    <h2> E-mail: {email} </h2>
    """

# Condicional que permite identificar el fichero principal
if __name__ == '__main__':
    app.run(debug=True)

print ("la suma de 5 + 6 es ",  5 + 6)